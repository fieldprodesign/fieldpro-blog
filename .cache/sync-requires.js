const { hot } = require("react-hot-loader/root")

// prefer default export if available
const preferDefault = m => (m && m.default) || m


exports.components = {
  "component---cache-dev-404-page-js": hot(preferDefault(require("/Users/setup/Learn-react/fieldpro-blog/.cache/dev-404-page.js"))),
  "component---src-pages-404-js": hot(preferDefault(require("/Users/setup/Learn-react/fieldpro-blog/src/pages/404.js"))),
  "component---src-pages-es-js": hot(preferDefault(require("/Users/setup/Learn-react/fieldpro-blog/src/pages/ES.js"))),
  "component---src-pages-fr-js": hot(preferDefault(require("/Users/setup/Learn-react/fieldpro-blog/src/pages/FR.js"))),
  "component---src-pages-index-js": hot(preferDefault(require("/Users/setup/Learn-react/fieldpro-blog/src/pages/index.js"))),
  "component---src-templates-blog-post-js": hot(preferDefault(require("/Users/setup/Learn-react/fieldpro-blog/src/templates/blogPost.js"))),
  "component---src-templates-category-js": hot(preferDefault(require("/Users/setup/Learn-react/fieldpro-blog/src/templates/category.js")))
}

