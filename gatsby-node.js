const path = require(`path`)

exports.createPages = ({ graphql, actions }) => {
  /* */
  const { createPage } = actions

  const blogPost = path.resolve(`./src/templates/blogPost.js`)
  const blogCategory = path.resolve(`./src/templates/category.js`)

  return graphql(
    `
      {
        allContentfulBlogPost {
          edges {
            node {
              slug
              title
              language
            }
          }
        }
        allContentfulBlogCategory {
          edges {
            node {
              language
              slug
              title
            }
          }
        }
      }
    `
  ).then(result => {
    if (result.errors) {
      throw result.errors
    }

    // Create blog posts pages.
    const posts = result.data.allContentfulBlogPost.edges

    posts.forEach((post, index) => {
      const previous = index === posts.length - 1 ? null : posts[index + 1].node
      const next = index === 0 ? null : posts[index - 1].node

      createPage({
        path: post.node.slug,
        component: blogPost,
        context: {
          slug: post.node.slug,
          language: post.node.language,
          previous,
          next,
        },
      })
    })

    // Create categories pages.
    const categories = result.data.allContentfulBlogCategory.edges

    categories.forEach((category, index) => {
      const previous =
        index === categories.length - 1 ? null : categories[index + 1].node
      const next = index === 0 ? null : categories[index - 1].node

      createPage({
        path: category.node.slug,
        component: blogCategory,
        context: {
          slug: category.node.slug,
          previous,
          next,
        },
      })
    })
  })
}
