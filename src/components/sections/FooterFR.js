import React from "react"
import styled from "styled-components"
import { Link } from "gatsby"

import Layout from "../Layout"
import { H3, P, HeaderGroup } from "../styles/TextStyles.js"
import {
  FooterContainer,
  FooterGroup,
  FooterLogo,
  LinkGroup,
  Copyright,
} from "../sections/Footer.js"

const FooterFR = () => {
  return (
    <Layout>
      <FooterContainer>
        <FooterGroup>
          <FooterLogo>
            <Link to="https://fieldproapp.com/fr">
              <img
                src={require("../../../static/images/optimetriks-logo.png")}
              />
            </Link>
          </FooterLogo>
          <LinkGroup>
            <H3>Product</H3>
            <Link to="https://fieldproapp.com/fr">Accueil</Link>
            <Link to="https://fieldproapp.com/sales-fr">Ventes</Link>
            <Link to="https://fieldproapp.com/retail-fr">Merchandising</Link>
            <Link to="https://fieldproapp.com/app-fr">Application Mobile</Link>
            <Link to="https://fieldproapp.com/analytics-fr">
              Rapports Analytiques
            </Link>
            <Link to="https://fieldproapp.com/pricing-fr">Prix</Link>
          </LinkGroup>
          <LinkGroup>
            <H3>Resources</H3>
            <Link to="/">Cas d’usage clients</Link>
            <Link to="https://fieldproapp.com/support-fr">Support</Link>
            <Link to="https://help.fieldproapp.com/?lang=fr">
              Base de connaissance
            </Link>
            <Link to="https://fieldproapp.com/terms-fr">
              Termes & Conditions
            </Link>
          </LinkGroup>
          <LinkGroup>
            <H3>Company</H3>
            <Link to="https://fieldproapp.com/about-fr">A propos de nous</Link>
            <Link to="/">Blog</Link>
            <Link to="https://fieldproapp.com/contact-fr">Contactez nous</Link>
            <Link to="https://optimetriks.factorialhr.com/">Recrutement</Link>
          </LinkGroup>
          <LinkGroup>
            <H3>Social</H3>
            <Link to="https://www.linkedin.com/company/optimetriks">
              LinkedIn
            </Link>
            <Link to="https://www.facebook.com/fieldproapp">Facebook</Link>
            <Link to="https://twitter.com/optimetriks">Twitter</Link>
            <Link to="https://www.youtube.com/channel/UC3lznqy3g-OCcUvci_FkVzg">
              YouTube
            </Link>
            <Link to="https://medium.com/@optimetriks">Medium</Link>
          </LinkGroup>
        </FooterGroup>
        <Copyright>
          <P>
            © 2020 | Optimetriks | Tous les droits sont réservés | Termes et
            conditions d'application
          </P>
        </Copyright>
      </FooterContainer>
    </Layout>
  )
}

export default FooterFR
