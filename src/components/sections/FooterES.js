import React from "react"
import styled from "styled-components"
import { Link } from "gatsby"

import Layout from "../Layout"
import { H3, P, HeaderGroup } from "../styles/TextStyles.js"
import {
  FooterContainer,
  FooterGroup,
  FooterLogo,
  LinkGroup,
  Copyright,
} from "../sections/Footer.js"

const FooterES = () => {
  return (
    <Layout>
      <FooterContainer>
        <FooterGroup>
          <FooterLogo>
            <Link to="https://fieldproapp.com/es">
              <img
                src={require("../../../static/images/optimetriks-logo.png")}
              />
            </Link>
          </FooterLogo>
          <LinkGroup>
            <H3>Product</H3>
            <Link to="https://fieldproapp.com/es">Inicio</Link>
            <Link to="https://fieldproapp.com/sales-es">Ventas</Link>
            <Link to="https://fieldproapp.com/retail-es">Auditoría Retail</Link>
            <Link to="https://fieldproapp.com/app-es">Aplicación Móvil</Link>
            <Link to="https://fieldproapp.com/analytics-es">
              Analíticas de Negocio
            </Link>
            <Link to="https://fieldproapp.com/pricing-es">Tarifas</Link>
          </LinkGroup>
          <LinkGroup>
            <H3>Resources</H3>
            <Link to="/case-studies">Casos de Estudio</Link>
            <Link to="https://fieldproapp.com/support-es">Soporte</Link>
            <Link to="https://help.fieldproapp.com/?lang=es">
              Conocimiento Base
            </Link>
            <Link to="https://fieldproapp.com/terms-es">
              Términos y condiciones
            </Link>
          </LinkGroup>
          <LinkGroup>
            <H3>Nosotros</H3>
            <Link to="https://fieldproapp.com/about-es">About Us</Link>
            <Link to="/">Blog</Link>
            <Link to="https://fieldproapp.com/contact-es">Contáctanos</Link>
            <Link to="https://optimetriks.factorialhr.com/">Carreras</Link>
          </LinkGroup>
          <LinkGroup>
            <H3>Social</H3>
            <Link to="https://www.linkedin.com/company/optimetriks">
              LinkedIn
            </Link>
            <Link to="https://www.facebook.com/fieldproapp">Facebook</Link>
            <Link to="https://twitter.com/optimetriks">Twitter</Link>
            <Link to="https://www.youtube.com/channel/UC3lznqy3g-OCcUvci_FkVzg">
              YouTube
            </Link>
            <Link to="https://medium.com/@optimetriks">Medium</Link>
          </LinkGroup>
        </FooterGroup>
        <Copyright>
          <P>
            © 2020 | Optimetriks | Reservados todos los derechos | Los términos
            y Condiciones aplican
          </P>
        </Copyright>
      </FooterContainer>
    </Layout>
  )
}

export default FooterES
