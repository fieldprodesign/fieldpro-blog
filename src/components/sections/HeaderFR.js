import React, { useEffect, useRef, useState } from "react"
import styled from "styled-components"
import { Link } from "gatsby"

import { HeaderGroup } from "../styles/TextStyles.js"
import {
  MenuContainer,
  MenuGroup,
  MenuLogo,
  MenuLangBtns,
  MenuIconContainer,
  MenuIcon,
  MenuLinks,
  YellowLink,
  MenuButton,
  DropDownContainer,
  DropDownButton,
  DropDownLabel,
  ProductMoreIcon,
  ProductLessIcon,
  RsrcsMoreIcon,
  RsrcsLessIcon,
  CompanyMoreIcon,
  CompanyLessIcon,
  DropDownGroup,
  ProductDropDown,
  RsrcsDropDown,
  CompanyDropDown,
} from "../sections/Header.js"

function HeaderFR() {
  /* */
  const [productIsOpen, setProductIsOpen] = useState(false)
  const [rsrcsIsOpen, setRsrcsIsOpen] = useState(false)
  const [companyIsOpen, setCompanyIsOpen] = useState(false)

  const [menuOpen, toggleMenuOpen] = useState(false)

  const [boxShadow, setBoxShadow] = useState(false)
  const navRef = useRef()
  navRef.current = boxShadow
  useEffect(() => {
    const handleScroll = () => {
      const show = window.scrollY > 50
      if (navRef.current !== show) {
        setBoxShadow(show)
      }
    }
    document.addEventListener("scroll", handleScroll)

    return () => {
      document.removeEventListener("scroll", handleScroll)
    }
  }, [])

  return (
    <HeaderGroup>
      <MenuContainer boxShadow={boxShadow}>
        <MenuGroup>
          <MenuLogo>
            <Link
              target="_parent"
              activeClassName="active"
              to="https://fieldproapp.com/fr"
            >
              <img
                src={require("../../../static/images/fieldpro-logo.png")}
                alt="logo"
              />
            </Link>
          </MenuLogo>

          <MenuLangBtns>
            <Link target="_parent" to="/">
              <img src={require("../../../static/images/flag-en.png")} />
            </Link>
            <Link target="_parent" to="/FR">
              <img src={require("../../../static/images/flag-fr.png")} />
            </Link>
            <Link target="_parent" to="/ES">
              <img src={require("../../../static/images/flag-es.png")} />
            </Link>
          </MenuLangBtns>
          <MenuLinks menuOpen={menuOpen}>
            <Link
              target="_parent"
              activeClassName="active"
              to="https://fieldproapp.com/fr"
            >
              <div class="darkGrey">Accueil</div>
            </Link>
            <DropDownContainer>
              <DropDownButton
                onClick={() => {
                  setProductIsOpen(!productIsOpen)
                  setRsrcsIsOpen(false)
                  setCompanyIsOpen(false)
                }}
              >
                <DropDownLabel>Produit</DropDownLabel>
                <ProductMoreIcon
                  productIsOpen={productIsOpen}
                  src={require("../../../static/images/expand_more.png")}
                />
                <ProductLessIcon
                  productIsOpen={productIsOpen}
                  src={require("../../../static/images/expand_less.png")}
                />
              </DropDownButton>
              <ProductDropDown productIsOpen={productIsOpen}>
                <DropDownGroup>
                  <Link
                    target="_parent"
                    activeClassName="active"
                    to="https://fieldproapp.com/sales-fr"
                    class="darkGrey"
                  >
                    Ventes
                  </Link>
                  <Link
                    target="_parent"
                    activeClassName="active"
                    to="https://fieldproapp.com/retail-fr"
                    class="darkGrey"
                  >
                    Merchandising
                  </Link>
                  <Link
                    target="_parent"
                    activeClassName="active"
                    to="https://fieldproapp.com/app-fr"
                    class="darkGrey"
                  >
                    App Mobile
                  </Link>
                  <Link
                    target="_parent"
                    activeClassName="active"
                    to="https://fieldproapp.com/analytics-fr"
                    class="darkGrey"
                  >
                    Rapports Analytiques
                  </Link>
                </DropDownGroup>
              </ProductDropDown>
            </DropDownContainer>
            <Link
              target="_parent"
              activeClassName="active"
              to="https://fieldproapp.com/pricing-fr"
            >
              <div class="darkGrey">Prix</div>
            </Link>
            <DropDownContainer>
              <DropDownButton
                onClick={() => {
                  setRsrcsIsOpen(!rsrcsIsOpen)
                  setProductIsOpen(false)
                  setCompanyIsOpen(false)
                }}
              >
                <DropDownLabel>Ressources</DropDownLabel>
                <RsrcsMoreIcon
                  rsrcsIsOpen={rsrcsIsOpen}
                  src={require("../../../static/images/expand_more.png")}
                />
                <RsrcsLessIcon
                  rsrcsIsOpen={rsrcsIsOpen}
                  src={require("../../../static/images/expand_less.png")}
                />
              </DropDownButton>
              <RsrcsDropDown rsrcsIsOpen={rsrcsIsOpen}>
                <DropDownGroup>
                  <Link
                    target="_parent"
                    activeClassName="active"
                    to="https://fieldproapp.com/support-fr"
                    class="darkGrey"
                  >
                    Support
                  </Link>
                  <Link
                    target="_parent"
                    activeClassName="active"
                    to="https://help.fieldproapp.com"
                    class="darkGrey"
                  >
                    Base de connaissances
                  </Link>
                  <Link
                    target="_parent"
                    activeClassName="active"
                    to="https://fieldproapp.com/testimonials-fr"
                    class="darkGrey"
                  >
                    Témoignages
                  </Link>
                  <Link
                    target="_parent"
                    activeClassName="active"
                    to="https://fieldproapp.com/terms-fr"
                    class="darkGrey"
                  >
                    Termes & Conditions
                  </Link>
                </DropDownGroup>
              </RsrcsDropDown>
            </DropDownContainer>
            <DropDownContainer>
              <DropDownButton
                onClick={() => {
                  setCompanyIsOpen(!companyIsOpen)
                  setProductIsOpen(false)
                  setRsrcsIsOpen(false)
                }}
              >
                <DropDownLabel>Entreprise</DropDownLabel>
                <CompanyMoreIcon
                  companyIsOpen={companyIsOpen}
                  src={require("../../../static/images/expand_more.png")}
                />
                <CompanyLessIcon
                  companyIsOpen={companyIsOpen}
                  src={require("../../../static/images/expand_less.png")}
                />
              </DropDownButton>
              <CompanyDropDown companyIsOpen={companyIsOpen}>
                <DropDownGroup>
                  <Link
                    target="_parent"
                    activeClassName="active"
                    to="https://fieldproapp.com/about-fr"
                    class="darkGrey"
                  >
                    A Propos
                  </Link>
                  <Link
                    target="_parent"
                    activeClassName="active"
                    to="https://blog.fieldproapp.com/FR"
                    class="darkGrey"
                  >
                    Cas clients
                  </Link>
                  <Link
                    target="_parent"
                    activeClassName="active"
                    to="https://optimetriks.factorialhr.com"
                    class="darkGrey"
                  >
                    Recrutement
                  </Link>
                </DropDownGroup>
              </CompanyDropDown>
            </DropDownContainer>
            <Link
              target="_parent"
              activeClassName="active"
              to="https://blog.fieldproapp.com/FR"
            >
              <div class="darkGrey">Blog</div>
            </Link>
            <YellowLink>
              <Link
                target="_parent"
                activeClassName="active"
                to="https://fieldproapp.com/contact-fr"
              >
                <div>Contact</div>
              </Link>
            </YellowLink>
            <MenuButton>
              <Link
                target="_parent"
                activeClassName="active"
                to="https://fieldproapp.com/schedule-demo-fr"
              >
                <button>Planifiez une démo</button>
              </Link>
            </MenuButton>
          </MenuLinks>
        </MenuGroup>
        <MenuIconContainer>
          <MenuIcon
            menuOpen={menuOpen}
            onClick={() => toggleMenuOpen(!menuOpen)}
          >
            <div />
            <div />
            <div />
          </MenuIcon>
        </MenuIconContainer>
      </MenuContainer>
    </HeaderGroup>
  )
}

export default HeaderFR
