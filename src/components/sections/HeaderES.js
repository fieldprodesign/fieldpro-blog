import React, { useEffect, useRef, useState } from "react"
import styled from "styled-components"
import { Link } from "gatsby"

import { HeaderGroup } from "../styles/TextStyles.js"
import {
  MenuContainer,
  MenuGroup,
  MenuLogo,
  MenuLangBtns,
  MenuIconContainer,
  MenuIcon,
  MenuLinks,
  YellowLink,
  MenuButton,
  DropDownContainer,
  DropDownButton,
  DropDownLabel,
  ProductMoreIcon,
  ProductLessIcon,
  RsrcsMoreIcon,
  RsrcsLessIcon,
  CompanyMoreIcon,
  CompanyLessIcon,
  DropDownGroup,
  ProductDropDown,
  RsrcsDropDown,
  CompanyDropDown,
} from "../sections/Header.js"

function HeaderES() {
  /* */
  const [productIsOpen, setProductIsOpen] = useState(false)
  const [rsrcsIsOpen, setRsrcsIsOpen] = useState(false)
  const [companyIsOpen, setCompanyIsOpen] = useState(false)

  const [menuOpen, toggleMenuOpen] = useState(false)

  const [boxShadow, setBoxShadow] = useState(false)
  const navRef = useRef()
  navRef.current = boxShadow
  useEffect(() => {
    const handleScroll = () => {
      const show = window.scrollY > 50
      if (navRef.current !== show) {
        setBoxShadow(show)
      }
    }
    document.addEventListener("scroll", handleScroll)

    return () => {
      document.removeEventListener("scroll", handleScroll)
    }
  }, [])

  return (
    <HeaderGroup>
      <MenuContainer boxShadow={boxShadow}>
        <MenuGroup>
          <MenuLogo>
            <Link
              target="_parent"
              activeClassName="active"
              to="https://fieldproapp.com/es"
            >
              <img
                src={require("../../../static/images/fieldpro-logo.png")}
                alt="logo"
              />
            </Link>
          </MenuLogo>

          <MenuLangBtns>
            <Link target="_parent" to="/">
              <img src={require("../../../static/images/flag-en.png")} />
            </Link>
            <Link target="_parent" to="/FR">
              <img src={require("../../../static/images/flag-fr.png")} />
            </Link>
            <Link target="_parent" to="/ES">
              <img src={require("../../../static/images/flag-es.png")} />
            </Link>
          </MenuLangBtns>
          <MenuLinks menuOpen={menuOpen}>
            <Link
              target="_parent"
              activeClassName="active"
              to="https://fieldproapp.com/es"
            >
              <div class="darkGrey">Inicio</div>
            </Link>
            <DropDownContainer>
              <DropDownButton
                onClick={() => {
                  setProductIsOpen(!productIsOpen)
                  setRsrcsIsOpen(false)
                  setCompanyIsOpen(false)
                }}
              >
                <DropDownLabel>Producto</DropDownLabel>
                <ProductMoreIcon
                  productIsOpen={productIsOpen}
                  src={require("../../../static/images/expand_more.png")}
                />
                <ProductLessIcon
                  productIsOpen={productIsOpen}
                  src={require("../../../static/images/expand_less.png")}
                />
              </DropDownButton>
              <ProductDropDown productIsOpen={productIsOpen}>
                <DropDownGroup>
                  <Link
                    target="_parent"
                    activeClassName="active"
                    to="https://fieldproapp.com/sales-es"
                    class="darkGrey"
                  >
                    Automatización de Ventas
                  </Link>
                  <Link
                    target="_parent"
                    activeClassName="active"
                    to="https://fieldproapp.com/retail-es"
                    class="darkGrey"
                  >
                    Auditoría Retail
                  </Link>
                  <Link
                    target="_parent"
                    activeClassName="active"
                    to="https://fieldproapp.com/app-es"
                    class="darkGrey"
                  >
                    App Móvil
                  </Link>
                  <Link
                    target="_parent"
                    activeClassName="active"
                    to="https://fieldproapp.com/analytics-es"
                    class="darkGrey"
                  >
                    Analíticas de Negocio
                  </Link>
                </DropDownGroup>
              </ProductDropDown>
            </DropDownContainer>
            <Link
              target="_parent"
              activeClassName="active"
              to="https://fieldproapp.com/pricing-es"
            >
              <div class="darkGrey">Tarifas</div>
            </Link>
            <DropDownContainer>
              <DropDownButton
                onClick={() => {
                  setRsrcsIsOpen(!rsrcsIsOpen)
                  setProductIsOpen(false)
                  setCompanyIsOpen(false)
                }}
              >
                <DropDownLabel>Recursos</DropDownLabel>
                <RsrcsMoreIcon
                  rsrcsIsOpen={rsrcsIsOpen}
                  src={require("../../../static/images/expand_more.png")}
                />
                <RsrcsLessIcon
                  rsrcsIsOpen={rsrcsIsOpen}
                  src={require("../../../static/images/expand_less.png")}
                />
              </DropDownButton>
              <RsrcsDropDown rsrcsIsOpen={rsrcsIsOpen}>
                <DropDownGroup>
                  <Link
                    target="_parent"
                    activeClassName="active"
                    to="https://fieldproapp.com/support"
                    class="darkGrey"
                  >
                    Soporte
                  </Link>
                  <Link
                    target="_parent"
                    activeClassName="active"
                    to="https://help.fieldproapp.com/?lang=es"
                    class="darkGrey"
                  >
                    Conocimiento Base
                  </Link>
                  <Link
                    target="_parent"
                    activeClassName="active"
                    to="https://fieldproapp.com/testimonials-es"
                    class="darkGrey"
                  >
                    Testimonios
                  </Link>
                  <Link
                    target="_parent"
                    activeClassName="active"
                    to="https://fieldproapp.com/terms-es"
                    class="darkGrey"
                  >
                    Términos y condiciones
                  </Link>
                </DropDownGroup>
              </RsrcsDropDown>
            </DropDownContainer>
            <DropDownContainer>
              <DropDownButton
                onClick={() => {
                  setCompanyIsOpen(!companyIsOpen)
                  setProductIsOpen(false)
                  setRsrcsIsOpen(false)
                }}
              >
                <DropDownLabel>Empresa</DropDownLabel>
                <CompanyMoreIcon
                  companyIsOpen={companyIsOpen}
                  src={require("../../../static/images/expand_more.png")}
                />
                <CompanyLessIcon
                  companyIsOpen={companyIsOpen}
                  src={require("../../../static/images/expand_less.png")}
                />
              </DropDownButton>
              <CompanyDropDown companyIsOpen={companyIsOpen}>
                <DropDownGroup>
                  <Link
                    target="_parent"
                    activeClassName="active"
                    to="https://fieldproapp.com/about-es"
                    class="darkGrey"
                  >
                    Nosotros
                  </Link>
                  <Link
                    target="_parent"
                    activeClassName="active"
                    to="https://blog.fieldproapp.com/?lang=es"
                    class="darkGrey"
                  >
                    Casos de Estudio
                  </Link>
                  <Link
                    target="_parent"
                    activeClassName="active"
                    to="https://optimetriks.factorialhr.com"
                    class="darkGrey"
                  >
                    Carreras
                  </Link>
                </DropDownGroup>
              </CompanyDropDown>
            </DropDownContainer>
            <Link
              target="_parent"
              activeClassName="active"
              to="https://blog.fieldproapp.com/?lang=es"
            >
              <div class="darkGrey">Blog</div>
            </Link>
            <YellowLink>
              <Link
                target="_parent"
                activeClassName="active"
                to="https://fieldproapp.com/contact-es"
              >
                <div>Contáctanos</div>
              </Link>
            </YellowLink>
            <MenuButton>
              <Link
                target="_parent"
                activeClassName="active"
                to="https://fieldproapp.com/schedule-demo-es"
              >
                <button>Reserva una Demo</button>
              </Link>
            </MenuButton>
          </MenuLinks>
        </MenuGroup>
        <MenuIconContainer>
          <MenuIcon
            menuOpen={menuOpen}
            onClick={() => toggleMenuOpen(!menuOpen)}
          >
            <div />
            <div />
            <div />
          </MenuIcon>
        </MenuIconContainer>
      </MenuContainer>
    </HeaderGroup>
  )
}

export default HeaderES
