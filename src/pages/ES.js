import React from "react"
import { Link, graphql } from "gatsby"
import styled from "styled-components"

import Layout from "../components/Layout.js"
import SEO from "../components/seo"

import HeaderES from "../components/sections/HeaderES.js"
import FooterES from "../components/sections/FooterES.js"
import {
  HeaderGroup,
  H1,
  H2,
  Caption,
} from "../components/styles/TextStyles.js"
import {
  CategoryMenuContainer,
  CategoryGroup,
  AllCategories,
  Category,
  Body,
  FeaturedPost,
  FeaturedImage,
  FeaturedText,
  FeaturedTitle,
  PostGroup,
  Post,
  PostImage,
  PostText,
  Title,
  CategoryTagContainer,
  CategoryTag,
} from "./index.js"

class BlogES extends React.Component {
  render() {
    const { data } = this.props
    const siteTitle = data.site.siteMetadata?.title || `Title`
    const posts = data.allContentfulBlogPost.edges

    return (
      <Layout location={this.props.location}>
        <SEO title="FieldPro Blog" keywords={[]} />
        <HeaderES />
        <HeaderGroup>
          <CategoryMenuContainer>
            <CategoryGroup>
              <AllCategories>
                <Link to="/ES" activeClassName="active">
                  All
                </Link>
              </AllCategories>
              {data.allContentfulBlogCategory.edges.map(({ node }) => {
                return (
                  <Category key={node.slug}>
                    <Link activeClassName="active" to={`/${node.slug}`}>
                      {node.title}
                    </Link>
                  </Category>
                )
              })}
            </CategoryGroup>
          </CategoryMenuContainer>
        </HeaderGroup>
        <Body>
          <FeaturedPost>
            {posts.map(({ node }) => {
              const title = node.title || node.slug
              if (node.featured) {
                return (
                  <div key={node.slug}>
                    <FeaturedImage>
                      <Link style={{ boxShadow: `none` }} to={`/${node.slug}`}>
                        <img src={node.image.fluid.src} />
                      </Link>
                    </FeaturedImage>
                    <FeaturedText>
                      <FeaturedTitle>
                        <Link
                          style={{ boxShadow: `none` }}
                          to={`/${node.slug}`}
                        >
                          {title}
                        </Link>
                      </FeaturedTitle>
                      <CategoryTagContainer>
                        {node.categories.map(categoryES => {
                          return (
                            <Link to={`/${categoryES.slug}`}>
                              <CategoryTag>
                                {categoryES.title}&#160;&#160;|
                              </CategoryTag>
                            </Link>
                          )
                        })}
                      </CategoryTagContainer>
                    </FeaturedText>
                  </div>
                )
              }
            })}
          </FeaturedPost>
          <PostGroup>
            {posts.map(({ node }) => {
              const title = node.title || node.slug
              if (node.featured) {
              } else {
                return (
                  <Post key={node.slug}>
                    <PostImage>
                      <Link style={{ boxShadow: `none` }} to={`/${node.slug}`}>
                        <img src={node.image.fluid.src} />
                      </Link>
                    </PostImage>
                    <PostText>
                      <Title>
                        <Link
                          style={{ boxShadow: `none` }}
                          to={`/${node.slug}`}
                        >
                          {title}
                        </Link>
                      </Title>
                      <CategoryTagContainer>
                        {node.categories.map(categoryES => {
                          return (
                            <Link to={`/${categoryES.slug}`}>
                              <CategoryTag>
                                {categoryES.title}&#160;&#160;|
                              </CategoryTag>
                            </Link>
                          )
                        })}
                      </CategoryTagContainer>
                    </PostText>
                  </Post>
                )
              }
            })}
          </PostGroup>
        </Body>
        <FooterES />
      </Layout>
    )
  }
}

export default BlogES

export const pageQuery = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }
    allContentfulBlogCategory(
      sort: { fields: updatedAt, order: ASC }
      filter: { language: { eq: "ES" } }
    ) {
      edges {
        node {
          title
          slug
        }
      }
    }
    allContentfulBlogPost(
      sort: { fields: createdAt, order: DESC }
      filter: { language: { eq: "ES" } }
    ) {
      edges {
        node {
          slug
          title
          featured
          image {
            fluid {
              src
            }
          }
          categories {
            title
            slug
          }
        }
      }
    }
  }
`
