import React from "react"
import { Link, graphql } from "gatsby"
import styled from "styled-components"

import Layout from "../components/Layout.js"
import SEO from "../components/seo"

import Header from "../components/sections/Header.js"
import HeaderFR from "../components/sections/HeaderFR.js"
import HeaderES from "../components/sections/HeaderES.js"
import Footer from "../components/sections/Footer.js"
import FooterFR from "../components/sections/FooterFR.js"
import FooterES from "../components/sections/FooterES.js"
import {
  HeaderGroup,
  H1,
  H2,
  Caption,
} from "../components/styles/TextStyles.js"
import {
  CategoryMenuContainer,
  CategoryGroup,
  AllCategories,
  Category,
  PostGroup,
  Post,
  PostImage,
  PostText,
  Title,
  CategoryTagContainer,
  CategoryTag,
} from "../pages/index.js"

class CategoryTemplate extends React.Component {
  render() {
    /* */
    const { data } = this.props
    const posts = data.allContentfulBlogPost.edges

    const category = this.props.data.contentfulBlogCategory

    var headerMenu
    if (category.language === "EN") {
      headerMenu = <Header />
    }
    if (category.language === "FR") {
      headerMenu = <HeaderFR />
    }
    if (category.language === "ES") {
      headerMenu = <HeaderES />
    }

    var footerMenu
    if (category.language === "EN") {
      footerMenu = <Footer />
    }
    if (category.language === "FR") {
      footerMenu = <FooterFR />
    }
    if (category.language === "ES") {
      footerMenu = <FooterES />
    }

    var AllLink
    if (category.language === "EN") {
      AllLink = (
        <AllCategories>
          <Link to="/" activeClassName="active">
            All
          </Link>
        </AllCategories>
      )
    }
    if (category.language === "FR") {
      AllLink = (
        <AllCategories>
          <Link to="/FR" activeClassName="active">
            All
          </Link>
        </AllCategories>
      )
    }
    if (category.language === "ES") {
      AllLink = (
        <AllCategories>
          <Link to="/ES" activeClassName="active">
            All
          </Link>
        </AllCategories>
      )
    }

    return (
      <Layout location={this.props.location}>
        <SEO title="FieldPro Blog" keywords={[]} />
        {headerMenu}
        <HeaderGroup>
          <CategoryMenuContainer>
            <CategoryGroup>
              {AllLink}
              {data.allContentfulBlogCategory.edges.map(({ node }) => {
                if (node.language === category.language) {
                  return (
                    <Category key={node.slug}>
                      <Link activeClassName="active" to={`/${node.slug}`}>
                        {node.title}
                      </Link>
                    </Category>
                  )
                }
              })}
            </CategoryGroup>
          </CategoryMenuContainer>
        </HeaderGroup>
        <Body>
          <PostGroup>
            {posts.map(({ node }) => {
              const title = node.title || node.slug
              return (
                <Post key={node.slug}>
                  <PostImage>
                    <Link style={{ boxShadow: `none` }} to={`/${node.slug}`}>
                      <img src={node.image.fluid.src} />
                    </Link>
                  </PostImage>
                  <PostText>
                    <Title>
                      <Link style={{ boxShadow: `none` }} to={`/${node.slug}`}>
                        {title}
                      </Link>
                    </Title>
                    <CategoryTagContainer>
                      {node.categories.map(category => {
                        return (
                          <Link to={`/${category.slug}`}>
                            <CategoryTag>
                              {category.title}&#160;&#160;|
                            </CategoryTag>
                          </Link>
                        )
                      })}
                    </CategoryTagContainer>
                  </PostText>
                </Post>
              )
            })}
          </PostGroup>
        </Body>
        {footerMenu}
      </Layout>
    )
  }
}

export default CategoryTemplate

export const categoryQuery = graphql`
  query CategoryBySlug($slug: String!) {
    site {
      siteMetadata {
        title
      }
    }
    contentfulBlogCategory(slug: { eq: $slug }) {
      language
      slug
      title
    }
    allContentfulBlogCategory(sort: { fields: updatedAt, order: ASC }) {
      edges {
        node {
          language
          title
          slug
        }
      }
    }
    allContentfulBlogPost(
      sort: { fields: createdAt, order: DESC }
      filter: { categories: { elemMatch: { slug: { eq: $slug } } } }
    ) {
      edges {
        node {
          slug
          title
          featured
          image {
            fluid {
              src
            }
          }
          categories {
            language
            title
            slug
          }
        }
      }
    }
  }
`

export const Body = styled.div`
  display: grid;
  justify-items: center;
  margin: 8em 0 0 0;
  padding: 0;
  @media (max-width: 32em) {
    margin: 10em 0 0 0;
  }
`
